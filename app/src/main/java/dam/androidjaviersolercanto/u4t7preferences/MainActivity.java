package dam.androidjaviersolercanto.u4t7preferences;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity {

    private final String MYPREFS = "MyPrefs";
    private EditText etPlayerName;
    private Spinner spinnerLevel;
    private EditText etScore;
    private Button btQuit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
    }

    private void setUI() {
        etPlayerName = findViewById(R.id.etPlayerName);

        // Level spinner, set adapter from string-array resource
        spinnerLevel = findViewById(R.id.spinnerLevel);
        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(this, R.array.levels, android.R.layout.simple_spinner_item);

        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        spinnerLevel.setAdapter(spinnerAdapter);

        etScore = findViewById(R.id.etScore);

        btQuit = findViewById(R.id.btQuit);
        btQuit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    // save activity data to preferences file
    @Override
    protected void onPause() {
        super.onPause();

        // get preference file
        SharedPreferences myPreferences = getSharedPreferences(MYPREFS, MODE_PRIVATE);

        // save UI data to file
        SharedPreferences.Editor editor = myPreferences.edit();

        editor.putString("PlayerName",etPlayerName.getText().toString());
        editor.putInt("Level",spinnerLevel.getSelectedItemPosition());
        editor.putInt("Score",Integer.parseInt(etScore.getText().toString()));

        editor.commit();
    }

    // read activity data from preferences file

    @Override
    protected void onResume() {
        super.onResume();

        // get preference file
        SharedPreferences myPreferences = getSharedPreferences(MYPREFS,MODE_PRIVATE);

        // set UI values reading data from file
        etPlayerName.setText(myPreferences.getString("PlayerName","uknonwn"));
        spinnerLevel.setSelection(myPreferences.getInt("Level",0));
        etScore.setText(String.valueOf(myPreferences.getInt("Score",0)));
    }
}
